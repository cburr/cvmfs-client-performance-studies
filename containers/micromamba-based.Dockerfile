FROM quay.io/centos/centos:stream9

RUN dnf install -y bzip2 && \
    mkdir -p /opt/micromamba/bin && \
    curl -Ls https://micro.mamba.pm/api/micromamba/linux-64/latest | tar -xvj -C /opt/micromamba/bin --strip-components=1 bin/micromamba && \
    /opt/micromamba/bin/micromamba shell init -p /opt/micromamba --shell bash && \
    dnf clean all && \
    rm -rf /var/cache/yum

COPY linux-64.yaml /root/env.yaml

RUN source /root/.bashrc && \
    micromamba create --yes --always-copy --file /root/env.yaml --name test-env && \
    micromamba clean --all --force-pkgs-dirs --yes

ENV PATH=/opt/micromamba/envs/test-env/bin:$PATH
