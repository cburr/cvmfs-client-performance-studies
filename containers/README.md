# Container images for testing

This directory contains recipes for building and testing images which don't depend on CVMFS for a few example workloads.

## LbEnv 2541

LbEnv is the login environment which every LHCb user sees when accessing lxplus.
It provides a variety of commands, most of which start new shells with environment variables set for accessing specific physics/analysis applications or grid middleware.

### Building

```bash
docker build containers/LbEnv-2541 -f containers/micromamba-based.Dockerfile -t gitlab-registry.cern.ch/cburr/cvmfs-client-performance-studies/lbenv-2541:latest
docker push gitlab-registry.cern.ch/cburr/cvmfs-client-performance-studies/lbenv-2541:latest
```

### Benchmarks

```bash
python -I -m LbEnv --sh --siteroot /cvmfs/fake.invalid
lb-dirac --help
lb-conda --help
lb-dev --help
lb-run --help  # NOTE: This requires /cvmfs/lhcb.cern.ch to be mounted else it hangs
lb-ap --help
python -c 'import matplotlib'
```

## LbConda default 2022-08-06_21-15

LbEnv is the login environment which every LHCb user sees when accessing lxplus.
It provides a variety of commands, most of which start new shells with environment variables set for accessing specific physics/analysis applications or grid middleware.

### Building

```bash
docker build containers/LbConda-default-2022-08-06_21-15 -f containers/micromamba-based.Dockerfile -t gitlab-registry.cern.ch/cburr/cvmfs-client-performance-studies/lbconda-default-2022-08-06_21-15:latest
docker push gitlab-registry.cern.ch/cburr/cvmfs-client-performance-studies/lbconda-default-2022-08-06_21-15:latest
```

### Benchmarks

```bash
root -l -b -q -x -e '1-1'
root -b -l -q -x /opt/micromamba/envs/test-env/tutorials/hsimple.C
ipython -c pass
python -c 'import ROOT'
python -c 'import matplotlib'
python -c 'import pandas'
snakemake --help
# Remaining tests require this to have been ran first
# echo $'#include <iostream>\nsize_t helloworld() {std::cout << "Hello world" << std::endl;return 0;} int main(int argc, char *argv[]) {helloworld();}' > /tmp/helloworld.cpp
root -l -b -q -x /tmp/helloworld.cpp
g++ /tmp/helloworld.cpp -o /tmp/helloworld
```
