# CVMFS Client Performance Studies

This repo contains scripts and containers for investigating areas where the performance of the CVMFS FUSE driver can be improved.
This work was originally shown at the [2022 CVMFS Users Workshop](https://indico.cern.ch/event/1079490/contributions/4939521/).
In this presentation two areas of optimisation were shown:

* **Cold cache**: The local machine's cache is completely clean. This is the primary source of problems.
* **Warm cache**: The local machine's cache contains the files but they haven't loaded into the kernel yet. This hasn't been investigated isn't seen as a priority.
* **Hot cache**: When running the same command repeatedly without clearing any caches CVMFS is still notably slower than local storage.

## Cold cache performance

When starting from a fresh machine many commands take a long time.
Within CERN this typically takes up to O(10) seconds for "basic" commands however outside of CERN this can take several minutes.
This can be reliability repeated with `cvmfs_config wipecache` being called in between so it appears to be a network latency issue rather than an issue with the proxy infrastructure.
Workarounds like pre-filling the cache at CERN are not very feasible as the biggest problem is on people's local machines and institute provided clusters (where network latency tends to be higher than within CERN).

It appears that the problem is mainly caused by processes accessing large numbers of unique files (hundreds, sometimes thousands).
These are sequentially downloaded by the CVMFS driver so the network latency to the proxy is compounded thousands of times.

### Possible solution

The best solution is likely to be able to tell CVMFS about certain common access patterns.

For example if `/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-run` is opened we know that 359 additional files are going to be needed within the next few hundred milliseconds corresponding (15MB of data, 5.5MB compressed). In this case `bin/python3.9` is also needed (22MB, 7MB compressed) however as this file is relatively large I presume it wouldn't be included in any bundled objects.

1. `bin/lb-run` is opened, CVMFS starts downloading `bin/lb-run` as well as a bundle of the 359 associated files
2. `python` is opened next, CVMFS returns it like normal
3. Later files are already warm (or maybe even hot and pre-loaded into the kernel) in the cache before they're even opened

For creating these bundles it can be done on the release manager by giving it a command to run while tracing. The file which triggers the bundle to be loaded can be given, for example:

```bash
/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-conda default/2022-09-20_13-08 python -c "import ROOT"
# Bundle starts with: /cvmfs/lhcbdev.cern.ch/conda/envs/default/2022-09-20_13-08/linux-64/lib/python3.9/site-packages/ROOT/__init__.py
# (ignore the fact this command is split across 2 repositories)
# Contains ~1400 files
# Maybe split into a few smaller bundles triggered by libCling.so etc.
```

```bash
/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/python -c "pass"
# Bundle starts with: /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/python3.9
# Contains ~48 files
```

```bash
/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-run --help
# Bundle starts with: /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-run
# Contains ~100 files but ignores the bin/python3.9 bundle (or triggers it directly)
```

```bash
/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-dev --help
# Bundle starts with: /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-run
# Contains ~100 files but ignores the bin/python3.9 bundle (or triggers it directly)
```

```bash
/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/python -c 'import matplotlib'
# Bundle starts with: /cvmfs/lhcbdev.cern.ch/conda/envs/default/2022-09-20_13-08/linux-64/lib/python3.9/site-packages/matplotlib/__init__.py
# Contains ~46 files
# Also triggers loading bundles for numpy/__init__.py (181 files) PIL/__init__.py (46 files) (and maybe others)
# Uses the bin/python3.9 bundle before matplotlib/__init__.py is touched
```

### Open questions/ideas

1. How is the consistency of the bundle the handled if other files are changed in later transactions (especially for gateway setups)?
2. Can we automatically split a bundle into several smaller reusable parts
  a. Example for LbEnv: `python`+`lb-run`+`lb-dev`+`lb-conda`
  b. Example for Python analysis environments: `numpy`+`matplotlib`+`ROOT`+`pandas`+...
3. Can bundles be triggered by a few starting files (e.g. `ROOT/__init__.py` and `ROOT/__pycache__/__init__.cpython-39.pyc`)

### Example

**Note:** This should really be done with CVMFS's own tracing to get more accurate results.

```bash
test_command=("/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/python" "-c" "import matplotlib")
sudo cvmfs_config wipecache
strace -T -f -e open,openat,execve -o /tmp/cold.trace "${test_command[@]}"
sudo sync; sudo bash -c 'echo 3 > /proc/sys/vm/drop_caches'
sleep 5
strace -T -f -e open,openat,execve -o /tmp/warm.trace "${test_command[@]}"
sleep 5
strace -T -f -e open,openat,execve -o /tmp/hot.trace "${test_command[@]}"

grep /cvmfs/lhcb.cern.ch /tmp/cold.trace | grep -v ENOENT | grep -v O_DIRECTORY | grep -v EROFS | sed -E 's@[^"]+"([^"]+)"[^<]+<([^>]+)>@\1@g' >/tmp/cold.files
grep /cvmfs/lhcb.cern.ch /tmp/cold.trace | grep -v ENOENT | grep -v O_DIRECTORY | grep -v EROFS | sed -E 's@[^"]+"([^"]+)"[^<]+<([^>]+)>@\2@g' >/tmp/cold.timing
grep /cvmfs/lhcb.cern.ch /tmp/warm.trace | grep -v ENOENT | grep -v O_DIRECTORY | grep -v EROFS | sed -E 's@[^"]+"([^"]+)"[^<]+<([^>]+)>@\1@g' >/tmp/warm.files
grep /cvmfs/lhcb.cern.ch /tmp/warm.trace | grep -v ENOENT | grep -v O_DIRECTORY | grep -v EROFS | sed -E 's@[^"]+"([^"]+)"[^<]+<([^>]+)>@\2@g' >/tmp/warm.timing
grep /cvmfs/lhcb.cern.ch /tmp/hot.trace | grep -v ENOENT | grep -v O_DIRECTORY | grep -v EROFS | sed -E 's@[^"]+"([^"]+)"[^<]+<([^>]+)>@\1@g' >/tmp/hot.files
grep /cvmfs/lhcb.cern.ch /tmp/hot.trace | grep -v ENOENT | grep -v O_DIRECTORY | grep -v EROFS | sed -E 's@[^"]+"([^"]+)"[^<]+<([^>]+)>@\2@g' >/tmp/hot.timing
echo "These hashes should all be the same"
md5sum /tmp/cold.files /tmp/warm.files /tmp/hot.files

echo "Opening files with a cold cache took $(awk '{ sum += $1 } END { print sum }' /tmp/cold.timing) seconds"
echo "Opening files with a warm cache took $(awk '{ sum += $1 } END { print sum }' /tmp/warm.timing) seconds"
echo "Opening files with a hot cache took $(awk '{ sum += $1 } END { print sum }' /tmp/hot.timing) seconds"

paste /tmp/cold.timing /tmp/warm.timing /tmp/hot.timing /tmp/hot.files > /tmp/summary.txt
```

### Containers for reproducing

See [`containers/README.md`](https://gitlab.cern.ch/cburr/cvmfs-client-performance-studies/-/blob/main/containers/README.md).

## Hot cache performance

This is a lower priority for now as it's less painful for users so leaving as TODO for now.
