#!/usr/bin/env python3
import pathlib
import shlex
import subprocess
import timeit
from functools import partial

try:
    from numpy import mean, std
except ImportError:
    print("Using hacky mean/std")
    mean = lambda x: sum(x) / len(x)
    std = lambda x: (sum((y - mean(x))**2 for y in x) / len(x))**0.5

def wipe_cache():
    subprocess.check_call(["sudo", "cvmfs_config", "wipecache"], stdout=subprocess.DEVNULL)

def do_thing(command):
    subprocess.check_call(command, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

commands = {
    "LbEnv setup": "/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/python -I -m LbEnv --sh --siteroot /cvmfs/lhcb.cern.ch/lib",
    "LHCbDIRAC setup": "source /cvmfs/lhcb.cern.ch/lhcbdirac/lhcbdirac",
    "Physics application setup": "PATH=/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin:$PATH /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-run --container=singularity --siteroot /cvmfs/lhcb.cern.ch/lib Gaudi/v36r7 echo",
    "Minimal Gaudi": "PATH=/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin:$PATH /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-run --container=singularity --siteroot /cvmfs/lhcb.cern.ch/lib Gaudi/v36r7 gaudirun.py",
    "lb-conda": "/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-conda default echo",
    "lb-ap": "/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-ap --help",
    "lhcb-proxy-init": "/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lhcb-proxy-init --help",
    "import ROOT": "PATH=/cvmfs/lhcbdev.cern.ch/conda/envs/default/2022-08-06_21-15/linux-64/bin:$PATH python -c 'import ROOT'",
    "import pydata": "/cvmfs/lhcbdev.cern.ch/conda/envs/default/2022-08-06_21-15/linux-64/bin/python -c 'import numpy, matplotlib, pandas'",
    "ROOT from conda": "PATH=/cvmfs/lhcbdev.cern.ch/conda/envs/default/2022-08-06_21-15/linux-64/bin:$PATH root -l -b -q -e '1-1'",
    "LCG view": "time source /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/setup.sh",
}
centos7_only = {
    "Physics application setup": "PATH=/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin:$PATH /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-run --disallow-containers --siteroot /cvmfs/lhcb.cern.ch/lib Gaudi/v36r7 echo",
    "Minimal Gaudi": "PATH=/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin:$PATH /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/2541/stable/linux-64/bin/lb-run --disallow-containers --siteroot /cvmfs/lhcb.cern.ch/lib Gaudi/v36r7 gaudirun.py",
    "ROOT from lcg": "time source /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/setup.sh; root -l -b -q -e '1-1'",
}

distro_info = {
    k: shlex.split(v)[0]
    for k, v in dict(
        x.split("=", 1)
        for x in pathlib.Path("/etc/os-release").read_text().split("\n")
        if x
    ).items()
}
if distro_info.get("REDHAT_SUPPORT_PRODUCT_VERSION") == "7":
    commands = {**commands, **centos7_only}
else:
    print("OS not detected as CentOS 7 like, some tests will not be ran")

for name, command in commands.items():
    print("***", name)
    print("***", command)
    cold_cache = timeit.repeat(setup=wipe_cache, stmt=partial(do_thing, command), number=1, repeat=10)
    hot_cache = timeit.repeat(stmt=partial(do_thing, command), number=1, repeat=10)
    print("cold", mean(cold_cache), std(cold_cache))
    print("hot", mean(hot_cache), std(hot_cache))
